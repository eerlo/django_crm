#-*- coding: utf-8 -*-

from core import models
from core import serializers
from rest_framework import viewsets


class CustomerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Customers to be viewed and edited.
    """
    queryset = models.Customer.objects.all()
    serializer_class = serializers.CustomerSerializer

