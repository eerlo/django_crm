# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('change_time', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=50, verbose_name='Name')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'City',
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='ContactHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('change_time', models.DateTimeField(auto_now=True)),
                ('contact_datetime', models.DateTimeField(default=datetime.datetime.now, verbose_name='Date/Time of Contact')),
                ('description', models.TextField(max_length=4000, verbose_name='Contact Description Details')),
            ],
            options={
                'ordering': ['creation_time'],
                'verbose_name': 'Contact History',
                'verbose_name_plural': 'Contact Historys',
            },
        ),
        migrations.CreateModel(
            name='ContactInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('change_time', models.DateTimeField(auto_now=True)),
                ('value', models.CharField(max_length=100, verbose_name='Value')),
            ],
            options={
                'ordering': ['pk'],
                'verbose_name': 'Customer Contact Info',
                'verbose_name_plural': 'Customers Contact Infos',
            },
        ),
        migrations.CreateModel(
            name='ContactReason',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('change_time', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=30, verbose_name='Name')),
                ('description', models.TextField(max_length=500, verbose_name='Description')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Contact Reason',
                'verbose_name_plural': 'Contact Reasons',
            },
        ),
        migrations.CreateModel(
            name='ContactType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Type Name')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Contact Type',
                'verbose_name_plural': 'Contact Types',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('change_time', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=50, verbose_name='Name')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Country',
                'verbose_name_plural': 'Countries',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('change_time', models.DateTimeField(auto_now=True)),
                ('first_name', models.CharField(max_length=50, verbose_name='First Name')),
                ('middle_name', models.CharField(max_length=50, null=True, verbose_name='Middle Name', blank=True)),
                ('last_name', models.CharField(max_length=50, null=True, verbose_name='Last Name', blank=True)),
                ('address', models.TextField(max_length=300, null=True, verbose_name='Address', blank=True)),
                ('city', models.ForeignKey(verbose_name='City', blank=True, to='core.City', null=True)),
            ],
            options={
                'ordering': ['first_name'],
                'verbose_name': 'Customes',
                'verbose_name_plural': 'Customers',
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('change_time', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=50, verbose_name='Name')),
                ('country', models.ForeignKey(verbose_name='Country', to='core.Country')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'State',
                'verbose_name_plural': 'States',
            },
        ),
        migrations.AddField(
            model_name='contactinfo',
            name='contact_type',
            field=models.ForeignKey(verbose_name='Type', to='core.ContactType'),
        ),
        migrations.AddField(
            model_name='contactinfo',
            name='customer',
            field=models.ForeignKey(related_name='contact_infos', verbose_name='Customer', to='core.Customer'),
        ),
        migrations.AddField(
            model_name='contacthistory',
            name='customer',
            field=models.ForeignKey(verbose_name='Customer', to='core.Customer'),
        ),
        migrations.AddField(
            model_name='contacthistory',
            name='reason',
            field=models.ForeignKey(verbose_name='Reason of Contact', to='core.ContactReason'),
        ),
        migrations.AddField(
            model_name='city',
            name='state',
            field=models.ForeignKey(verbose_name='State', to='core.State'),
        ),
    ]
