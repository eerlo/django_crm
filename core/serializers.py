#-*- coding: utf-8 -*-

from core import models
from rest_framework import serializers


class ContactInfoSerializer(serializers.ModelSerializer):
    contact_type = serializers.StringRelatedField()
    class Meta:
        model = models.ContactInfo
        fields = (u'contact_type', u'value',)


class CustomerSerializer(serializers.ModelSerializer):
    contact_infos = ContactInfoSerializer(many=True)
    city = serializers.StringRelatedField()
    class Meta:
        model = models.Customer
        fields = (u'first_name', u'middle_name', u'last_name',
                  u'city', u'address', u'contact_infos')

