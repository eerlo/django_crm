#-*- coding: utf-8 -*-

from django.db import models
from datetime import datetime


class BaseModel(models.Model):
    """
    Base model with all the commom attributes and methods.
    """
    creation_time = models.DateTimeField(auto_now_add=True)
    change_time = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Country(BaseModel):
    """
    Model for Country
    """
    name = models.CharField(u'Name',
                            max_length=50)

    class Meta:
        verbose_name = u'Country'
        verbose_name_plural = u'Countries'
        ordering = [u'name']

    def __unicode__(self):
        return self.name


class State(BaseModel):
    """
    Model for State(of a Country)
    """
    name = models.CharField(u'Name',
                            max_length=50)
    country = models.ForeignKey(Country,
                                verbose_name=u'Country')

    class Meta:
        verbose_name = u'State'
        verbose_name_plural = u'States'
        ordering = [u'name']

    def __unicode__(self):
        return u'%s(%s)' % (self.name, self.country)


class City(BaseModel):
    """
    Model for City(of a State)
    """
    name = models.CharField(u'Name',
                            max_length=50)
    state = models.ForeignKey(State,
                              verbose_name=u'State')
    class Meta:
        verbose_name = u'City'
        verbose_name_plural = u'Cities'
        ordering = [u'name']

    def __unicode__(self):
        return u'%s (%s - %s)' % (self.name, self.state.name,
                                  self.state.country.name)


class Customer(BaseModel):
    """
    Model for a Customer
    """
    first_name = models.CharField(u'First Name',
                                  max_length=50)
    middle_name = models.CharField(u'Middle Name',
                                  max_length=50,
                                  null=True,
                                  blank=True)
    last_name = models.CharField(u'Last Name',
                                 max_length=50,
                                 null=True,
                                 blank=True)
    city = models.ForeignKey(City,
                             verbose_name=u'City',
                             null=True,
                             blank=True)
    address = models.TextField(u'Address',
                               max_length=300,
                               null=True,
                               blank=True)

    class Meta:
        verbose_name = u'Customes'
        verbose_name_plural = u'Customers'
        ordering = [u'first_name']

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name,)


class ContactType(models.Model):
    name = models.CharField(u'Type Name',
                            max_length=50)

    class Meta:
        verbose_name = u'Contact Type'
        verbose_name_plural = u'Contact Types'
        ordering = [u'name']

    def __unicode__(self):
        return self.name


class ContactInfo(BaseModel):
    customer = models.ForeignKey(Customer,
                                 verbose_name=u'Customer',
                                 related_name=u'contact_infos')
    contact_type = models.ForeignKey(ContactType,
                                     verbose_name=u'Type')
    value = models.CharField(u'Value',
                             max_length=100)

    class Meta:
        verbose_name = u'Customer Contact Info'
        verbose_name_plural = u'Customers Contact Infos'
        ordering = [u'pk']

    def __unicode__(self):
        return '%s - %s:%s' % (self.customer, self.contact_type, self.value)


class ContactReason(BaseModel):
    """
    Model for a Customer Contact Reason
    """
    name = models.CharField(u'Name',
                            max_length=30)
    description = models.TextField(u'Description',
                                   max_length=500)

    class Meta:
        verbose_name = u'Contact Reason'
        verbose_name_plural = u'Contact Reasons'
        ordering = [u'name']

    def __unicode__(self):
        return self.name


class ContactHistory(BaseModel):
    """
    Model for a Contact History
    """
    customer = models.ForeignKey(Customer,
                                 verbose_name=u'Customer')
    contact_datetime = models.DateTimeField(u'Date/Time of Contact',
                                            default=datetime.now)
    reason = models.ForeignKey(ContactReason,
                               verbose_name=u'Reason of Contact')
    description = models.TextField(u'Contact Description Details',
                                   max_length=4000)

    class Meta:
        verbose_name = u'Contact History'
        verbose_name_plural = u'Contact Historys'
        ordering = [u'creation_time']

    def __unicode__(self):
        return '%s - %s' % (self.customer, self.creation_time)

