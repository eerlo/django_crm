#-*- coding: utf-8 -*-
from django.contrib import admin
from core import models


class StateInline(admin.TabularInline):
    model = models.State
    extra = 0

class CityInline(admin.TabularInline):
    model = models.City
    extra = 0

class CountryAdmin(admin.ModelAdmin):
    model = models.Country
    list_display = (u'name',)
    inlines = (StateInline,)


class StateAdmin(admin.ModelAdmin):
    model = models.State
    list_display = (u'name', u'country')
    inlines = (CityInline,)

class CityAdmin(admin.ModelAdmin):
    model = models.State
    list_display = (u'name', u'state')

class ContactHistoryInline(admin.StackedInline):
    model = models.ContactHistory
    extra = 0

class ContactTypeAdmin(admin.ModelAdmin):
    model = models.ContactType
    list_display = (u'name',)

class ContactInfoInline(admin.StackedInline):
    model = models.ContactInfo
    extra = 0

class CustomerAdmin(admin.ModelAdmin):
    model = models.Customer
    list_display = (u'first_name', u'middle_name', u'last_name', u'city')
    inlines = (ContactInfoInline, ContactHistoryInline,)

class ContactReasonAdmin(admin.ModelAdmin):
    model = models.ContactReason
    list_display = (u'name', u'description')


admin.site.register(models.Country, CountryAdmin)
admin.site.register(models.State, StateAdmin)
admin.site.register(models.City, CityAdmin)
admin.site.register(models.Customer, CustomerAdmin)
admin.site.register(models.ContactReason, ContactReasonAdmin)
admin.site.register(models.ContactType, ContactTypeAdmin)

