# Djagno CRM Software

This is a very simple CRM software build with django, using the django-admin for
UI.

To install and get it running, just create and activate a virtualenv,
then run:

`pip install -r requirements.txt`

`python manage.py syncdb`

`python manage.py loaddata core/fixtures/initial_data.json`

`python manage.py runserver`



# Tips


We already have some initial data in the initial_data.json fixture in the core
app.

The customer model can be acessed in a json api.
